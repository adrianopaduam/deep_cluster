import os
import time

import numpy as np
from keras.layers.core import Reshape
from keras.callbacks import ModelCheckpoint
from keras.applications.resnet50 import ResNet50

from Dataset import load

def train():

    #Loading Preprocessed Data
    X_train = load('X_train')
    X_test  = load('X_test')
    y_train = load('y_train')
    y_test  = load('y_test')

    for image in X_train:
        print(image.shape)

    #Model Creation
    model = ResNet50(weights=None,
                     input_shape=(224, 224, 3),
                     include_top=True,
                     classes=2)
    #Model Compilation
    model.compile(optimizer='Adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])


    #Loading Initial Weights
    filename = 'cg-resnet50-init_weights.npy'
    if os.path.exists(filename):
        print("Loading ResNet50 initial weights from %s ..." %(filename))
        init_weights = np.load(filename)
    else:
        print("Generating ResNet50 initial weigths ...")
        init_weights = model.get_weights()
        print("Saving ResNet50 initial weights into %s ..." %(filename))
        np.save(filename, init_weights)


    #Checkpoints Definition
    checkpoints_folder = os.path.abspath('./Checkpoints')
    if not os.path.exists(checkpoints_folder):
        os.makedirs(checkpoints_folder)

    path = checkpoints_folder +'/weights-cg-resnet50-Adam-{epoch:02d}-{val_acc:.2f}.h5'

    checkpointer = ModelCheckpoint(filepath=path,
                                   monitor='val_acc',
                                   verbose=0,
                                   save_best_only=True,
                                   save_weights_only=True,
                                   mode='auto')
    
    callbacks_list = [checkpointer]

    

    #Model Train
    history = []
    model.set_weights(init_weights)
    start = time.time()

    h = model.fit(X_train,
              y_train,
              validation_data=(X_test, y_test),
              epochs=200,
              batch_size=64,
              verbose=1,
              callbacks=callbacks_list)

    end = time.time()
    history.append(h)
    
    #Time Consumption
    total_time = end - start
    print("Train Total Time: %.4f" % total_time)


    #Data Recording
    np.save('./trained_weights.npy', model.get_weights())
    np.save('./train_history.npy', history)


if __name__ == '__main__':
    train()