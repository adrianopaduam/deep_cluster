def sendMail(subject, message):
    import smtplib
    from email.mime.text import MIMEText

    email = 'adriano.souza.1990@gmail.com'

    smtp = smtplib.SMTP_SSL('smtp.gmail.com', 465)

    smtp.login(email, _decode('abc', 'w4XDi8OVw4bDlsOSwpLCksKTwpE='))

    from_addr, to_addrs = email, [email]

    mime = MIMEText(message)
    mime['From'] = from_addr
    mime['To'] = ', '.join(to_addrs)
    mime['Subject'] = subject

    msg = mime.as_string()
    
    smtp.sendmail(from_addr, to_addrs, msg)

    smtp.close()
    
def _decode(key, enc):
    import base64
    dec = []
    enc = base64.urlsafe_b64decode(enc)
    for i in range(len(enc.decode())):
        key_c = key[i % len(key)]
        dec_c = chr((256 + ord(enc.decode()[i]) - ord(key_c)) % 256)
        dec.append(dec_c)
    return "".join(dec)

if __name__ == '__main__':
    import sys
    if(len(sys.argv) < 3):
        sys.exit("You must pass SUBJECT and MESSAGE\n")

    subject = sys.argv[1]
    message = sys.argv[2]

    sendMail(subject, message)

