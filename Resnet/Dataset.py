#Dataset mini-library: functions for dataset creation and data modelling

#############################################################################
# Dataset Creation                                                          #
# Usage: X, y = create_dataset(image_dir_path, remove=["OASIS"])            #
#############################################################################
def create_dataset(image_dir_path = './', remove=[]):
    import os
    import glob
    import numpy as np
    import nibabel as nib

    #######################################################################

    print("\nReading Images and Masks:")

    #Image Reading
    list_of_dirs = os.listdir(os.path.abspath(image_dir_path + '/Imagens'))

    #Optional Dataset Removal
    for dataset in remove:
        try:
            list_of_dirs.remove(dataset)
        except:
            continue
    print("Images: " + str(list_of_dirs))

    #Paths Reading
    home_dir = os.getcwd()
    file_paths = []

    os.chdir(image_dir_path + '/Imagens')
    for dir_name in list_of_dirs:

        os.chdir('./' + dir_name)

        for file_name in glob.glob('*.nii.gz'):
            file_paths.append(os.path.abspath(file_name))

        os.chdir('../')
    os.chdir(home_dir)

    #Image Allocation
    input_data = []
    for file in file_paths:
        input_data.append(nib.load(file).get_data().astype('int16'))
        if input_data[-1].ndim == 3:
            input_data[-1] = input_data[-1].reshape(input_data[-1].shape + (1,))

    #Bordering Images
    input_data = makeBorder(input_data)

    ##############################################################################

    #Mask Reading
    list_of_dirs = os.listdir(os.path.abspath(image_dir_path + '/Mascaras/'))

    #Optional Masks Removal
    for mascara in remove:
        try:
            list_of_dirs.remove(mascara)
        except:
            continue
    print("Masks: " + str(list_of_dirs))

    #Paths Reading
    home_dir = os.getcwd()
    file_paths = []

    os.chdir(image_dir_path + '/Mascaras/')
    for dir_name in list_of_dirs:

        os.chdir('./' + dir_name)

        for file_name in glob.glob('*.nii.gz'):
            file_paths.append(os.path.abspath(file_name))

        os.chdir('../')
    os.chdir(home_dir)

    #Masks Allocation
    output_data = []
    for file in file_paths:
        output_data.append(nib.load(file).get_data().astype('int16'))
        if output_data[-1].ndim == 3:
            output_data[-1] = output_data[-1].reshape(output_data[-1].shape + (1,))

    return input_data, output_data


##################################################################################
#Dataset Border Insertion                                                        #
#Usage: make_border(list_of_images)                                              #
##################################################################################
def makeBorder(dataset):
    import numpy as np
    from cv2 import copyMakeBorder

    bordered_dataset = []
    for image in dataset:

        top, bottom, left, right = 26, 27, 26, 27
        borderType, value = 0, (0,0,0)
        src = image[:,:,:,0]

        parcial = copyMakeBorder(src = src,
                               top = top,
                               bottom = bottom,
                               left = left,
                               right = right,
                               borderType = borderType, 
                               value = value)

        top, bottom = 0, 0
        x, y, z = parcial.shape
        z += 53

        result = np.zeros((x, y, z), dtype = 'int16')

        for image in range(parcial.shape[1]):
            sheet = copyMakeBorder(src = parcial[:,image,:],
                           top = top,
                           bottom = bottom,
                           left = left,
                           right = right,
                           borderType = borderType, 
                           value = value)
            result[:,image,:] = sheet

        result = result.reshape(result.shape + (1,))
        bordered_dataset.append(result)

    return bordered_dataset

##################################################################################
#Dataset Cubification Only                                                       #
#Usage: make_squares(dataset)                                                     #
##################################################################################
def make_squares(dataset, step=[2,2,26], four_dimensions=True):
    import time
    import numpy as np
    from sys import warnoptions
    from warnings import simplefilter
    if not warnoptions: simplefilter(action = 'ignore', category = RuntimeWarning)
    
    begin = time.time()
    
    squares = []
    i = 1
    
    print("\nCreating Squares:")
    print("Number of Images: %d" % len(dataset))
    
    for image in dataset:
        print("Image %02d" % i, end=' ')
        
        if four_dimensions:
            #Orientation Definition
            if image.shape[0] == image.shape[1]:
                w, h, d = image.shape[:-1]
                orientation = 1
            elif image.shape[0] == image.shape[2]:
                w, d, h = image.shape[:-1]
                orientation = 2
            else:
                d, w, h = image.shape[:-1]
                orientation = 3

            #Splitting Image in squares (Jump of 26 pixels)
            for depth in range(26, d - 27, step[0]):
                for height in range(26, h - 27, step[1]):
                    for width in range(26, w - 27, step[2]):
                        if orientation == 1:
                            squares.append(image[(width-26):(width+27),
                                                 (height-26):(height+27),
                                                 depth, 0])
                        elif orientation == 2:
                            squares.append(image[(width-26):(width+27),
                                                 depth, 
                                                 (height-26):(height+27), 0])
                        else:
                            squares.append(image[depth, 
                                                 (width-26):(width+27),
                                                 (height-26):(height+27), 0])
        else:
            #Orientation Definition
            if image.shape[0] == image.shape[1]:
                w, h, d = image.shape
                orientation = 1
            elif image.shape[0] == image.shape[2]:
                w, d, h = image.shape
                orientation = 2
            else:
                d, w, h = image.shape
                orientation = 3
                
            #Splitting Image in squares (Jump of 26 pixels)
            for height in range(26, h - 27, step[1]):
                for width in range(26, w - 27, step[2]):
                    if orientation == 1:
                        squares.append(image[(width-26):(width+27),
                                             (height-26):(height+27),
                                             0])
                    elif orientation == 2:
                        squares.append(image[(width-26):(width+27),
                                             0, 
                                             (height-26):(height+27)])
                    else:
                        squares.append(image[0, 
                                             (width-26):(width+27),
                                             (height-26):(height+27)])

        #Creating 'RGB' squares
        RGB_squares = []
        counter = 0
        while counter < len(squares):
            rgb_square = np.zeros((53,53,3), dtype='int16')
            
            rgb_square[:,:,0] = squares[counter]
            rgb_square[:,:,1] = squares[counter]
            rgb_square[:,:,2] = squares[counter]
            counter += 1
            
            RGB_squares.append(rgb_square)
            
        i += 1
        print("---------------> Loaded")
        
    print("Process Successfully Terminated")
    
    print("Number of lines: %d" % len(RGB_squares))

    end = time.time()
    print("Total Time: %f" % (end - begin))
    
    return RGB_squares

##################################################################################
#Masks Linearization Only                                                        #
#Usage: linearize_masks(masks)                                                   #
##################################################################################
def linearize_masks(masks, step=[2,2,26], two_classes=True):
    import time
    
    begin = time.time()
    
    linear_y = []
    pixel = 0
    i = 1
    
    print("\nLinearizing Masks:")
    print("Number of Masks: %d" % len(masks))
    
    for mask in masks:
        print("Image %02d" % i, end=' ')
        
        #Orientation Definition
        if mask.shape[0] == mask.shape[1]:
            w, h, d = mask.shape[:-1]
            orientation = 1
        elif mask.shape[0] == mask.shape[2]:
            w, d, h = mask.shape[:-1]
            orientation = 2
        else:
            d, w, h = mask.shape[:-1]
            orientation = 3
        
        #Linearization
        if(two_classes):
            for depth in range(0, d, step[0]):
                for height in range(0, h, step[1]):
                    for width in range(0, w, step[2]):
                        if    orientation == 1: pixel = mask[width, height, depth, 0]
                        elif  orientation == 2: pixel = mask[width, depth, height, 0]
                        else: pixel = mask[depth, width, height, 0]
                            
                        #Creation of 2 Classes (Brain and Non Brain)
                        linear_y.append([pixel, int(pixel == 0)])

        else:
            for depth in range(0, d, step[0]):
                for height in range(0, h, step[1]):
                    for width in range(0, w, step[2]):
                        if    orientation == 1: pixel = mask[width, height, depth, 0]
                        elif  orientation == 2: pixel = mask[width, depth, height, 0]
                        else: pixel = mask[depth, width, height, 0]
                            
                        #Creation of 1 Class Only
                        linear_y.append(pixel)

        i += 1
        print("---------------> Loaded")
        
    print("Process Successfully Terminated")
    
    print("Number of lines: %d" % len(linear_y))
    
    end = time.time()
    print("Total Time: %f" % (end - begin))
    
    return linear_y

##################################################################################
#Saving Preprocessed Images on File                                              #
#Usage: save(images, name)                                                       #
##################################################################################
def save(images, name):
    import os
    import numpy as np

    names = ['X_train', 'X_test', 'y_train', 'y_test', 'X_total', 'y_total']
    if name not in names:
        sys.exit("Not existent data type. Try name in \
            ['X_train', 'X_test', 'y_train', 'y_test', 'X_total', 'y_total']")

    print("\nSaving " + name + ': ', end='')

    x_dir = './Imagens/X'
    if not os.path.exists(x_dir):
        os.makedirs(x_dir)

    y_dir = './Mascaras/y'
    if not os.path.exists(y_dir):
        os.makedirs(y_dir)

    if name == names[0]:
        actual_dir = os.path.join(x_dir, 'train')
        if not os.path.exists(actual_dir):
            os.makedirs(actual_dir)

    elif name == names[1]:
        actual_dir = os.path.join(x_dir, 'test')
        if not os.path.exists(actual_dir):
            os.makedirs(actual_dir)
    
    elif name == names[2]:
        actual_dir = os.path.join(y_dir, 'train')
        if not os.path.exists(actual_dir):
            os.makedirs(actual_dir)

    elif name == names[3]:
        actual_dir = os.path.join(y_dir, 'test')
        if not os.path.exists(actual_dir):
            os.makedirs(actual_dir)
            
    elif name == names[4]:
        actual_dir = os.path.join(x_dir, 'total')
        if not os.path.exists(actual_dir):
            os.makedirs(actual_dir)
    
    elif name == names[5]:
        actual_dir = os.path.join(y_dir, 'total')
        if not os.path.exists(actual_dir):
            os.makedirs(actual_dir)

    for i in range(len(images)):
        save_path = os.path.join(actual_dir,'%d.npy'%i )
        np.save(save_path, images[i])

    print('Done')


##################################################################################
#Loading Preprocessed Images                                                     #
#Usage: load(type_name)                                                          #
##################################################################################
def load(type_name, two_classes=True):
    import os
    import sys
    from glob import glob
    
    import numpy as np

    names = ['X_train', 'X_test', 'X_total', 'y_train', 'y_test', 'y_total']
    if type_name not in names:
        sys.exit("Not existent data type. Try name in \
            ['X_train', 'X_test', 'y_train', 'y_test', 'X_total', 'y_total']")

    print('\nLoading ' + type_name + ': ', end='')

    home_dir = os.getcwd()
    data_paths = []

    if type_name == names[0]:
        os.chdir('./Imagens/X/train')
    elif type_name == names[1]:
        os.chdir('./Imagens/X/test')
    elif type_name == names[2]:
        os.chdir('./Imagens/X/total')
    elif type_name == names[3]:
        os.chdir('./Mascaras/y/train')
    elif type_name == names[4]:
        os.chdir('./Mascaras/y/test')
    elif type_name == names[5]:
        os.chdir('./Mascaras/y/total')
    
    num_images = len(glob('*.npy'))
    
    for i in range(num_images):
        file_name = os.path.join(os.path.abspath('./'), str(i) + '.npy')
        data_paths.append(file_name)

    os.chdir(home_dir)   

    index = 0
    if type_name in names[3:]:
        if two_classes:
            data_files = np.zeros((num_images, 2), dtype='int8')
        else:
            data_files = np.zeros((num_images), dtype='int8')
            
        for path in data_paths:
            data_files[index] = np.load(path).astype('int8')
            index += 1
    else:
        data_files = np.zeros((num_images, 224, 224, 3), dtype='int8')
        for path in data_paths:
            data_files[index] = np.load(path).astype('int8')
            index += 1

    print('Done')

    return data_files