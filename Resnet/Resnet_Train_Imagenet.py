from sys import warnoptions
from warnings import simplefilter
if not warnoptions: simplefilter(action = 'ignore', category = FutureWarning)

import os
from time import time
from traceback import print_exc

import numpy as np

from keras.layers.core import Reshape
from keras.callbacks import ModelCheckpoint
from keras.applications.resnet50 import ResNet50

from sklearn import svm
from sklearn.externals import joblib
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import confusion_matrix,accuracy_score

from Dataset import load
from SendMail import sendMail
#from DataPlot import *

# %load_ext autoreload
# %autoreload 2
# %matplotlib inline

np.random.seed(1)
os.environ['CUDA_VISIBLE_DEVICES'] = '0'

try:
    #Dataset Load
    X = load('X_total')
    y = load('y_total', two_classes=False)


    #Shapes Test
    print()
    print(X.shape, X[0].dtype)
    print(y.shape, y.dtype)
    print()


    # Balance of Occurrency Test
    zeros, ones = 0, 0
    for pixel in y:
        if pixel == 0: zeros += 1
        else : ones += 1
    print("Zeros: %d, Ones: %d\n" % (zeros, ones))


    #Model Creation
    base_model = ResNet50(weights='imagenet', 
                     input_shape=(224,224,3),
                     include_top=False)

    #Generating base Features
    filename = './resnet50features.npy'
    if os.path.exists(filename):
        print("Loading ResNet50 extracted features from %s ...\n" %(filename))
        resnet50features = np.load(filename)
    else:
        print("Extracting features from ResNet50 layers ...\n")
        resnet50features = base_model.predict(X)
        print("Saving ResNet50 extracted features into %s ...\n" %(filename))
        np.save(filename, resnet50features)

    resnet50features = np.reshape(resnet50features,(resnet50features.shape[0],-1))


    # Creating Stratified K-fold Subsets
        # skfind[i][0] -> train indices, skfind[i][1] -> test indices
    kfold = 5 # no. of folds
    skf = StratifiedKFold(kfold, shuffle=True,random_state=1)
    skfind = [None] * kfold
    cnt = 0
    for index in skf.split(X, y):
        skfind[cnt] = index
        cnt += 1

    # Initializing the Confusion Matrix
    conf_mat = np.zeros((2,2)) #Brain X Not-Brain


    accuracy = []
    start = time()
    for i in range(kfold):
        #Train and Test Indices
        train_indices = skfind[i][0]
        test_indices = skfind[i][1]

        #Train Definition
        X_train = resnet50features[train_indices]
        y_train = y[train_indices]

        #Test Definition
        X_test = resnet50features[test_indices]
        y_test = y[test_indices]

#        #Creating Support Vector Machine - (SCORE ESTIMATOR)
#        svr = svm.SVC()
#
#        #Dictionary of Parameters of Search
#        param_grid = [{'kernel': ['rbf'], 
#                       'gamma': [1e-4,1e-3],
#                       'C': [1,10,100,1000,10000]}]
#
#        #Top Model Creation
#        top_model = GridSearchCV(svr, param_grid, verbose=True, n_jobs=3)
#
#        #Top Model Training
#        history.append(top_model.fit(X_train,y_train))
#
#        #Top Model Test
#        y_pred = top_model.predict(X_test)
#
#        #Storing Accuracy Scores
#        local_accuracy = "[%d] Test acurracy: %.4f" %(i,accuracy_score(y_test,y_pred))
#        accuracy.append(local_accuracy)
#        print(local_accuracy)
#
#        #Adding Results to Global Confusion Matrix
#        cm = confusion_matrix(y_test,y_pred)
#        conf_mat = conf_mat + cm

        #Creating Support Vector Machine - (SCORE ESTIMATOR)
        svr = svm.SVC(kernel='rbf', gamma=1e-4)

        #Training
        svr.fit(X_train,y_train)

        #Top Model Test
        y_pred = svr.predict(X_test)

        #Storing Accuracy Scores
        local_accuracy = "[%d] Test acurracy: %.4f" %(i,accuracy_score(y_test,y_pred))
        accuracy.append(local_accuracy)
        print(local_accuracy)

        #Adding Results to Global Confusion Matrix
        cm = confusion_matrix(y_test,y_pred)
        conf_mat = conf_mat + cm

    end = time()

    #Saving History and Confusion Matrix
    np.save("resnet_imagenet_accuracy.npy", accuracy)
    np.save("resnet_imagenet_confusion_matrix.npy", conf_mat)
    
    #Dumping SVC into a pickle file
    joblib.dump(svr, 'trained_svc_model.pk1')

    #Accuracy Results Compilation
    counter = 0
    for result in accuracy:
        print("Train %d: %s" % (counter, result))
        counter == 1


    # Average Accuracy Compilation
    avg_acc = np.trace(conf_mat)/len(y)
    print("Average acurracy: %.4f" %(avg_acc))


    #Time Consumption
    total_time = end - start
    h = total_time // 3600
    m = (total_time // 60) - (h * 60)
    s = total_time - (m * 60) - (h * 3600)
    print("Total Time (Train): %dh% dm %ds" % (h, m, s))

    sendMail("Resnet Train - Complete", "Your train has been succesfully completed!")
    
except Exception as e:
    print("\n\nErro de Execução!!\n")
    print(e, end="\n\n")
    print(type(e))
    print_exc()
    sendMail("Resnet Train - Fail", "Your train has presented an error!\n\n" + str(e))
    
