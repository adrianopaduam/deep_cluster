from sys import warnoptions
from warnings import simplefilter
if not warnoptions: simplefilter(action = 'ignore', category = FutureWarning)

from PIL import Image
from sklearn.model_selection import train_test_split
from Dataset import create_dataset, save
from Dataset import make_squares, linearize_masks

import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'

import numpy as np
np.random.seed(1)

def resnet_preprocessing():

    #Dataset Creation (Only 1 Image)
    X, y = create_dataset(remove=['X', 'y'])
    X = make_squares(X)
    y = linearize_masks(y, two_classes=False)
    
    #Transforming X=(Numpy Arrays) to Xpil=(PIL Images)
    Xpil = []
    for image in X:
        tmp = image/np.abs(image.max())
        tmp *= (255.0/tmp.max())
        Xpil.append(Image.fromarray(np.uint8(tmp)))
    X = 0
    
    #Reshaping PIL Images
    Xreshaped = []
    for image in Xpil:
        Xreshaped.append(image.resize(size=(224,224)))
    Xpil = 0
    
    #Returning to Numpy Arrays
    Xfinal = []
    for image in Xreshaped:
        Xfinal.append(np.asarray(image))
    Xreshaped = 0

    #Dataset Splitting
    X_train, X_test, y_train, y_test = train_test_split(Xfinal, y, test_size=1/3, random_state=0)
    
    save(Xfinal, 'X_total')
    save(y, 'y_total')

if __name__ == '__main__':
    resnet_preprocessing()
