import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.cm as colormap
import matplotlib.ticker as ticker

np.random.seed(1)
plt.style.use('seaborn-whitegrid')

def plot_acc(history, save_figure=False):
    figure = plt.gcf()
    figure.set_size_inches(14, 6)
    ax = plt.subplot()
    plt.title('Accuracy')
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    colors = iter(colormap.gist_rainbow(np.linspace(0, 1, len(history))))
    for i in range(len(history)):
        color=next(colors)
        plt.plot(history[i].history['acc'], label='Train '+str(i), color=color, linestyle = 'solid', linewidth=2.0)
        plt.plot(history[i].history['val_acc'], label='Test '+str(i), color=color, linestyle = 'dotted', linewidth=2.0)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0.0,1.0))
    plt.legend()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.grid(True)
    
    if(save_figure):
        import os
        if not os.path.exists('./Figuras'):
            os.makedirs('./Figuras')
        
        figure.savefig('./Figuras/accuracy.png', bbox_inches='tight')
        plt.close(figure)
    else:
        plt.show()

def plot_loss(history, save_figure=False):
    figure = plt.gcf()
    figure.set_size_inches(14, 6)
    ax = plt.subplot()
    plt.title('Loss')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    colors = iter(colormap.gist_rainbow(np.linspace(0, 1, len(history))))
    for i in range(len(history)):
        color=next(colors)
        plt.plot(history[i].history['loss'], label='Train '+str(i), color=color, linestyle = 'solid', linewidth=2.0)
        plt.plot(history[i].history['val_loss'], label='Test '+str(i), color=color, linestyle = 'dotted', linewidth=2.0)
    plt.legend()
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.grid(True)
    
    if(save_figure):
        import os
        if not os.path.exists('./Figuras'):
            os.makedirs('./Figuras')
        
        figure.savefig('./Figuras/loss.png', bbox_inches='tight')
        plt.close(figure)
    else:
        plt.show()


def plot_mean_acc(history, save_figure=False):
    train_scores = np.zeros((len(history),len(history[0].history['acc'])))
    for fold in range(len(history)):
        train_scores[fold] = history[fold].history['acc']
    test_scores = np.zeros((len(history),len(history[0].history['val_acc'])))
    for fold in range(len(history)):
        test_scores[fold] = history[fold].history['val_acc']
    epochs = np.linspace(0, len(history[0].history['acc']), len(history[0].history['acc']))
    train_scores_mean = np.mean(train_scores, axis=0)
    train_scores_std = np.std(train_scores, axis=0)
    test_scores_mean = np.mean(test_scores, axis=0)
    test_scores_std = np.std(test_scores, axis=0)
    
    figsize=(14, 6)
    text_fontsize="medium"
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    plt.title('Mean Acc')
    ax.set_xlabel("Epoch", fontsize=text_fontsize)
    ax.set_ylabel("Score", fontsize=text_fontsize)
    ax.grid(True)
    ax.yaxis.set_major_locator(ticker.MultipleLocator(0.1))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax.fill_between(epochs, train_scores_mean - train_scores_std,
                    train_scores_mean + train_scores_std, alpha=0.1, color="r")
    ax.fill_between(epochs, test_scores_mean - test_scores_std,
                    test_scores_mean + test_scores_std, alpha=0.1, color="g")
    ax.plot(epochs, train_scores_mean, '-', color="r", linewidth=2.0, label="Train")
    ax.plot(epochs, test_scores_mean, '-', color="g", linewidth=2.0, label="Test")
    ax.tick_params(labelsize=text_fontsize)
    ax.legend(loc="best", fontsize=text_fontsize)
    x1,x2,y1,y2 = plt.axis()
    plt.axis((x1,x2,0.0,1.0))
    
    if(save_figure):
        import os
        if not os.path.exists('./Figuras'):
            os.makedirs('./Figuras')
        
        figure.savefig('./Figuras/mean_accuracy.png', bbox_inches='tight')
        plt.close(figure)
    else:
        plt.show()
    
def plot_mean_loss(history, save_figure=False):
    train_scores = np.zeros((len(history),len(history[0].history['loss'])))
    for fold in range(len(history)):
        train_scores[fold] = history[fold].history['loss']
    test_scores = np.zeros((len(history),len(history[0].history['val_loss'])))
    for fold in range(len(history)):
        test_scores[fold] = history[fold].history['val_loss']
    epochs = np.linspace(0, len(history[0].history['loss']), len(history[0].history['loss']))
    train_scores_mean = np.mean(train_scores, axis=0)
    train_scores_std = np.std(train_scores, axis=0)
    test_scores_mean = np.mean(test_scores, axis=0)
    test_scores_std = np.std(test_scores, axis=0)
    
    figsize=(14, 6)
    text_fontsize="medium"
    fig, ax = plt.subplots(1, 1, figsize=figsize)
    plt.title('Mean Loss')
    ax.set_xlabel("Epoch", fontsize=text_fontsize)
    ax.set_ylabel("Score", fontsize=text_fontsize)
    ax.grid(True)
    ax.yaxis.set_major_locator(ticker.MultipleLocator(1))
    ax.xaxis.set_major_locator(ticker.MultipleLocator(10))
    ax.fill_between(epochs, train_scores_mean - train_scores_std,
                    train_scores_mean + train_scores_std, alpha=0.1, color="r")
    ax.fill_between(epochs, test_scores_mean - test_scores_std,
                    test_scores_mean + test_scores_std, alpha=0.1, color="g")
    ax.plot(epochs, train_scores_mean, '-', color="r", linewidth=2.0, label="Train")
    ax.plot(epochs, test_scores_mean, '-', color="g", linewidth=2.0, label="Test")
    ax.tick_params(labelsize=text_fontsize)
    ax.legend(loc="best", fontsize=text_fontsize)
    
    if(save_figure):
        import os
        if not os.path.exists('./Figuras'):
            os.makedirs('./Figuras')
        
        figure.savefig('./Figuras/mean_loss.png', bbox_inches='tight')
        plt.close(figure)
    else:
        plt.show()
    
def plot_conf_mat(conf_mat, save_figure=False):    
    plt.style.use('seaborn-whitegrid')

    print("Plotting the confusion matrix")
    figure = plt.gcf()
    figure.set_size_inches(4, 3)
    sns.set(font_scale=1.25)
    hm = sns.heatmap(conf_mat, cbar=False, annot=True, square=True,
                     fmt='.0f', annot_kws={'size': 14}, linewidth = 0.2, cmap = 'binary',
                     yticklabels=['Non-Brain', 'Brain'], xticklabels=['Non-Brain', 'Brain'])
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    
    if(save_figure):
        import os
        if not os.path.exists('./Figuras'):
            os.makedirs('./Figuras')
        
        figure.savefig('./Figuras/confusion_matrix.png', bbox_inches='tight')
        plt.close(figure)
    else:
        plt.show()
    
def plot_conf_mat_normalized(conf_mat, save_figure=False):
    plt.style.use('seaborn-whitegrid')

    print("Plotting the confusion matrix normalized")
    conf_mat_norm = conf_mat/np.sum(conf_mat,axis=1)  # Normalizing the confusion matrix
    conf_mat_norm = np.around(conf_mat_norm,decimals=2)  # rounding to display in figure

    figure = plt.gcf()
    figure.set_size_inches(4, 3)
    sns.set(font_scale=1.25)
    hm = sns.heatmap(conf_mat_norm, cbar=False, annot=True, square=True,
                     fmt='.2f', annot_kws={'size': 14}, linewidth = 0.1, cmap = 'binary',
                     yticklabels=['Non-Brain', 'Brain'], xticklabels=['Non-Brain', 'Brain'])
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    
    if(save_figure):
        import os
        if not os.path.exists('./Figuras'):
            os.makedirs('./Figuras')
        
        figure.savefig('./Figuras/confusion_matrix_normalized.png', bbox_inches='tight')
        plt.close(figure)
    else:
        plt.show()