import os
import sys

import numpy as np
import nibabel as nib

from Dataset import makeBorder, editDataset
from cythonyze import cubify_cython
from Model import createCNN
from Util import delinearize



def readImages(list_of_paths):

    img_paths, images = [], []

    if os.path.isdir(list_of_paths[0]):
        img_paths = os.listdir(os.path.abspath(list_of_paths))
    else:
        for path in list_of_paths:
            img_paths.append(os.path.abspath(path))

    names = []
    for path in img_paths:
        names.append(os.path.split(path)[0] + '_MASK')

    for file in img_paths:
        images.append(nib.load(file).get_data())

        if len(images[-1].shape) < 3:
            sys.exit("cnn_strip.readImage(): Wrong Image Size. Shape must be >= 3")
        if len(images[-1].shape) == 3:
            images[-1] = images[-1].reshape(images[-1].shape + (1,))

    shapes = []
    for image in images:
        shapes.append(image.shape)

    return makeBorder(images), names, shapes


def skullStrip(list_of_images, list_of_shapes):

    model = createCNN()

    model.compile()
    model.compile(optimizer='ADAM', 
                  loss='kullback_leibler_divergence', 
                  metrics=['accuracy'])

    train_weights = np.load('trained_weights.npy')
    model.set_weights(train_weights)

    results = []
    total = len(list_of_images)
    for image in range(total):

        cubes = cubify_cython(list_of_images[image])

        linear_mask = model.predict(cubes)

        mask = delinearize(linear_mask, list_of_shapes[image])

        results.append(mask)

    return results


if __name__ == '__main__':

    if len(sys.argv) < 2:
        sys.exit("No image to strip. Usage: cnn_strip.py <image>")

    images, names, shapes = readImages(list(sys.argv[2:]))

    stripped = skullStrip(images, shapes)

    assert len(names) == len(stripped)

    for mask in len(stripped):
        nii_img = nib.Nifti1Image(stripped[mask], np.eye(4))
        nib.save(nii_img, os.path.join(os.getcwd(), names[mask] + '.nii.gz'))

