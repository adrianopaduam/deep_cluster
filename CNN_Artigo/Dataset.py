#Dataset mini-library: functions for dataset creation and data modelling

#############################################################################
# Dataset Creation                                                          #
# Usage: X, y = createDataset(image_dir_path, remove=["OASIS"])             #
#############################################################################
def createDataset(image_dir_path = './', remove=[]):
    import os
    import glob
    import numpy as np
    import nibabel as nib

    #######################################################################

    #Image Reading
    list_of_dirs = os.listdir(os.path.abspath(image_dir_path + '/Imagens'))

    #Optional Dataset Removal
    for dataset in remove:
        list_of_dirs.remove(dataset)
    print("Datasets: " + str(list_of_dirs))

    #Paths Reading
    home_dir = os.getcwd()
    file_paths = []

    os.chdir(image_dir_path + '/Imagens')
    for dir_name in list_of_dirs:

        os.chdir('./' + dir_name)

        for file_name in glob.glob('*.nii.gz'):
            file_paths.append(os.path.abspath(file_name))

        os.chdir('../')
    os.chdir(home_dir)

    #Image Allocation
    input_data = []
    for file in file_paths:
        input_data.append(nib.load(file).get_data())
        if input_data[-1].ndim == 3:
            input_data[-1] = input_data[-1].reshape(input_data[-1].shape + (1,))

    #Bordering Images
    input_data = makeBorder(input_data)

    ##############################################################################

    #Mask Reading
    list_of_dirs = os.listdir(os.path.abspath(image_dir_path + '/Mascaras/'))

    #Optional Masks Removal
    for mascara in remove:
        list_of_dirs.remove(mascara)
    print("Mascaras: " + str(list_of_dirs))

    #Paths Reading
    home_dir = os.getcwd()
    file_paths = []

    os.chdir(image_dir_path + '/Mascaras/')
    for dir_name in list_of_dirs:

        os.chdir('./' + dir_name)

        for file_name in glob.glob('*.nii.gz'):
            file_paths.append(os.path.abspath(file_name))

        os.chdir('../')
    os.chdir(home_dir)

    #Masks Allocation
    output_data = []
    for file in file_paths:
        output_data.append(nib.load(file).get_data())
        #output_data[-1] = np.rot90(output_data[-1], 1, (0,1))
        if output_data[-1].ndim == 3:
            output_data[-1] = output_data[-1].reshape(output_data[-1].shape + (1,))

    return input_data, output_data


##################################################################################
#Dataset Border Insertion                                                        #
#Usage: make_border(list_of_images)                                              #
##################################################################################
def makeBorder(dataset):
    import numpy as np
    from cv2 import copyMakeBorder

    bordered_dataset = []
    for image in dataset:

        top, bottom, left, right = 26, 27, 26, 27
        borderType, value = 0, (0,0,0)
        src = image[:,:,:,0]

        parcial = copyMakeBorder(src = src,
                               top = top,
                               bottom = bottom,
                               left = left,
                               right = right,
                               borderType = borderType, 
                               value = value)

        top, bottom = 0, 0
        x, y, z = parcial.shape
        z += 53

        result = np.zeros((x, y, z), dtype = 'int16')

        for image in range(parcial.shape[1]):
            sheet = copyMakeBorder(src = parcial[:,image,:],
                           top = top,
                           bottom = bottom,
                           left = left,
                           right = right,
                           borderType = borderType, 
                           value = value)
            result[:,image,:] = sheet

        result = result.reshape(result.shape + (1,))
        bordered_dataset.append(result)

    return bordered_dataset

##################################################################################
#Dataset Pre-Processing (Cubification and Linearization)                         #
#Usage: edit_dataset(list_of_images, 'cubify'|'linearize)                        #
##################################################################################
def editDataset(dataset, edit_type):
    from cythonyze import cubify_cython, linearize_cython
    import time

    begin = time.time()

    if edit_type == "cubify":
        name = "Image"
    elif edit_type == "linearize":
        name = "Mask"
    else:
        import sys
        sys.exit("\nDataset.edit_dataset(): Wrong Edition Type. Try 'cubify' or 'linearize'")

    print("\nNumber of %ss: %d" % (name, len(dataset)))

    i = 1
    edited = []
    for image in dataset:
        print("%s %02d" % (name, i), end=' ')

        if edit_type == "cubify":
            edited += cubify_cython(image)
        else:
            edited += linearize_cython(image)

        i += 1
        print("---------------> Loaded")

    print("Process Successfully Terminated")

    print("Number of lines: %d" % len(edited))

    end = time.time()
    print("Total Time: %f\n" % (end - begin))

    return edited
