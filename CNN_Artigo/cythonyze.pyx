import numpy as np
cimport numpy as np

def cubify_cython(np.ndarray[np.int16_t, ndim=4] image):
    
    cdef int w, h, d
    cdef int depth, height, width
    cdef int orientation
       
    cdef int img_x = image.shape[0]
    cdef int img_y = image.shape[1]
    cdef int img_z = image.shape[2]
    
    if (img_x == img_y):
        w, h, d = img_x, img_y, img_z
        orientation = 1
    elif (img_x == img_z):
        w, d, h = img_x, img_y, img_z
        orientation = 2
    else:
        d, w, h = img_x, img_y, img_z
        orientation = 3
    
    cubes = []
    for depth in range(26, d - 27):
        for height in range(26, h - 27):
            for width in range(26, w - 27):
                if orientation == 1:
                    cubes.append(image[(width-26):(width+27),
                                       (height-26):(height+27),
                                       (depth-26):(depth+27), 0])
                elif orientation == 2:
                    cubes.append(image[(width-26):(width+27),
                                       (depth-26):(depth+27), 
                                       (height-26):(height+27), 0])
                else:
                    cubes.append(image[(depth-26):(depth+27), 
                                       (width-26):(width+27),
                                       (height-26):(height+27), 0])
                    
    return cubes



def linearize_cython(np.ndarray[np.int16_t, ndim=4] mask):
    cdef int w, h, d
    cdef int depth, height, width
    cdef int orientation
    
    #Orientation Definition
    cdef int msk_x = mask.shape[0]
    cdef int msk_y = mask.shape[1]
    cdef int msk_z = mask.shape[2]
    
    if (msk_x == msk_y):
        w, h, d = msk_x, msk_y, msk_z
        orientation = 1
    elif (msk_x == msk_z):
        w, d, h = msk_x, msk_y, msk_z
        orientation = 2
    else:
        d, w, h = msk_x, msk_y, msk_z
        orientation = 3
        
    linear_y = []
    for depth in range(26, d - 27):
        for height in range(26, h - 27):
            for width in range(26, w - 27):
                if    orientation == 1: linear_y.append(mask[width, height, depth, 0])
                elif  orientation == 2: linear_y.append(mask[width, depth, height, 0])
                else: linear_y.append(mask[depth, width, height, 0])

    return linear_y


def delinearize_cython(np.ndarray[np.int16_t, ndim=4] mask, shape):
    cdef int w, h, d
    cdef int depth, height, width
    cdef int orientation
    
    #Orientation Definition
    cdef int msk_x = shape[0]
    cdef int msk_y = shape[1]
    cdef int msk_z = shape[2]
    
    if (msk_x == msk_y):
        w, h, d = msk_x, msk_y, msk_z
        orientation = 1
    elif (msk_x == msk_z):
        w, d, h = msk_x, msk_y, msk_z
        orientation = 2
    else:
        d, w, h = msk_x, msk_y, msk_z
        orientation = 3
        
    #Delinearization
    cdef np.ndarray[np.int16_t, ndim=4] new_mask = np.zeros(shape, dtype='int16')
    for depth in range(d):
        for height in range(h):
            for width in range(w):
                if   orientation == 1: new_mask[width, height, depth] = mask[(depth-1)*h*w + (height-1)*w + width]
                elif orientation == 2: new_mask[width, depth, height] = mask[(depth-1)*h*w + (height-1)*w + width]
                else:                  new_mask[depth, width, height] = mask[(depth-1)*h*w + (height-1)*w + width]
        
    return new_mask