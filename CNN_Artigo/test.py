import os
import time

import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score

from Dataset import createDataset, editDataset
from Model import createCNN
from SendMail import sendMail

np.random.seed(1)
os.environ['CUDA_VISIBLE_DEVICES'] = '1'


def main():
    try:
        #Data Creation
        X, y = createDataset(remove=['OASIS', 'LPBA40'])
        X = editDataset(X, 'cubify')
        y = editDataset(y, 'linearize')

	#Data Fix
        deleted = 0
        for i in range(len(X)):
            i -= deleted
            if X[i].shape != (53,53,53):
                del X[i]
                del y[i]
                deleted += 1

        #Data Splitting
        X_train, X_test, y_train, y_test = train_test_split(X, y,
                                                        test_size=1/3,
                                                        random_state=0)

        #Model Creation
        model = createCNN()

        #Model Compilation
        model.compile(optimizer='ADAM', 
                      loss='kullback_leibler_divergence', 
                      metrics=['accuracy'])

        #Trained Weights Load
        train_weights = np.load('trained_weights.npy')
        model.set_weights(train_weights)

        #Test Prediction
        begin = time.time()
        y_pred = model.predict(X_test, verbose=1)
        end = time.time()

        #Accuracy and Time Consumption
        accuracy = accuracy_score(y_test,y_pred)
        total_time = end-start
        print("Test acurracy: %.4f Train time: %.4f s" % (acurracy,total_time))

        #Saivng Test Results (for further analisys)
        np.save("./test_prediction.npy", y_pred)

        #Mail Advisor (Success)
        sendMail("test.py - Finished", "You can now see the results.\n\nTest is over.")

    except:
        #Mail Advisor (Fail)
        sendMail("test.py - Error", "There was an error in test.py execution.\n\nTest was Interrupted.")


if __name__ == "__main__":
    main()
