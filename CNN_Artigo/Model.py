#Model mini-library: functions for neural network creation

from keras import Sequential
from keras.layers import Conv2D, Conv3D
from keras.layers import MaxPooling2D, MaxPooling3D
from keras.layers import Flatten, Dropout
from keras.layers import Dense
from keras.callbacks import ModelCheckpoint
from keras.models import Model

#Model from "J. Kleesiek et al" Article
def createCNN():
    model = Sequential()
    model.add(Conv3D(16, (4,4,4), input_shape=(53,53,53,1), activation='relu'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2)))
    model.add(Conv3D(24, (5,5,5), activation='relu'))
    model.add(Conv3D(28, (5,5,5), activation='relu'))
    model.add(Conv3D(34, (5,5,5), activation='relu'))
    model.add(Conv3D(42, (5,5,5), activation='relu'))
    model.add(Conv3D(50, (5,5,5), activation='relu'))
    model.add(Conv3D(50, (5,5,5), activation='relu'))
    model.add(Flatten())
    model.add(Dense(2, activation='softmax'))
    
    return model

#VGG16 model for 2D images
def createVGG16(input_shape=(256,256,1)):
    model = Sequential()
    model.add(Conv2D(64, (3,3), data_format="channels_last", activation='relu', padding='same', input_shape=input_shape))
    model.add(Conv2D(64, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(128, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(128, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(256, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(256, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(256, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(512, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(512, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(512, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Conv2D(512, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(512, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv2D(512, (3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling2D(pool_size=(2, 2), strides=(2, 2)))
    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dense(4096, activation='relu'))
    model.add(Dense(2, activation='softmax'))
    
    return model    


#VGG16 model for 3D images
def createVGG3D(input_shape=(309,309,181,1)):
    model = Sequential()
    model.add(Conv3D(64, (3,3,3), data_format="channels_last", activation='relu', padding='same', input_shape=input_shape))
    model.add(Conv3D(64, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2)))
    model.add(Conv3D(128, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(128, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2)))
    model.add(Conv3D(256, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(256, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(256, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2)))
    model.add(Conv3D(512, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(512, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(512, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2)))
    model.add(Conv3D(512, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(512, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(Conv3D(512, (3,3,3), data_format="channels_last", activation='relu', padding='same'))
    model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2, 2, 2)))
    model.add(Flatten())
    model.add(Dense(4096, activation='relu'))
    model.add(Dense(4096, activation='relu'))
    model.add(Dense(2, activation='softmax'))
    
    return model